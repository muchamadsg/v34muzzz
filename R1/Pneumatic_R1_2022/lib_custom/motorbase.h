#ifndef motorbase_h
#define motorbase_h

void motorbase_init();
void motorbase_maju(int speed);
void motorbase_mundur(int speed);
void motorbase_kanan(int speed);
void motorbase_kiri(int speed);
void motorbase_putarkanan(int speed);
void motorbase_putarkiri(int speed);
void motorbase_stop();

#endif
