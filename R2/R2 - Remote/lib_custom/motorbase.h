#ifndef motorbase_h
#define motorbase_h

//void motorbase_init();
//void motorbase_init(pin_type motor1_pin[2], pwm_type motor1_pwm, pin_type motor2_pin[2], pwm_type motor2_pwm, pin_type motor3_pin[2], pwm_type motor3_pwm, pin_type motor4_pin[2], pwm_type motor4_pwm);
void motorbase_move();
void motorbase_maju(int speed);
void motorbase_mundur(int speed);
void motorbase_kanan(int speed);
void motorbase_kiri(int speed);
void motorbase_putarkanan(int speed);
void motorbase_putarkiri(int speed);
void motorbase_stop();

#endif
