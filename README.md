Veamus on KRI 2022
---

Updated on Friday, May 29th 2022

Reupload


### R1 (omni wheel) folder contains:

- 'R1 - Remote' untuk pergerakan motorbase dengan remote PS3
- 'sketch_KRAI_2022_PS3BT_Sekat' untuk kontrol remote PS3
- 'Pelempar_Manual' untuk pelempar bola

### R2 (mecanum wheel) folder contains:

- 'R2 - Remote' untuk pergerakan motorbase dengan remote PS3
- 'sketch_KRAI_2022_PS3BT_Uno_Basic' untuk kontrol remote PS3

### Library:

- Library untuk shield PS3 Arduino, di file USB_Host
- LIbrary untuk aplikasi Dabble Android/Iphone, silahkan download di - https://thestempedia.com/docs/dabble/game-pad-module/