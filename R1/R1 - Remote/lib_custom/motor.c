#include "tm_stm32f4_gpio.h"
#include "tm_stm32f4_pwm.h"
#include "tm_stm32f4_timer_properties.h"
#include "motor.h"

void motor_init(pwm_type pwm, pin_type pin[2]) {
	// inisialisasi pwm untuk roda
	TM_PWM_InitTimer(pwm.TIM, pwm.TIM_Data, pwm.Timer);

	// inisialisasi setiap roda dengan 2 pin digital untuk setting maju dan mundur serta satu pin analog untuk mengatur kecepatan motor
	TM_GPIO_Init(pin[0].GPIO, pin[0].Pin, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);
	TM_GPIO_Init(pin[1].GPIO, pin[1].Pin, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);
	TM_PWM_InitChannel(pwm.TIM_Data, pwm.Channel, pwm.PinsPack);
}

void motor_set_speed(pwm_type pwm, pin_type pin[2], float percentage){ //masukkan kecepatan dalam bentuk persen -100% sampai 100%
	// error handling supaya kecepatan yang dimasukkan tidak bisa kurang dari -100% atau lebih dari 100%
	if (percentage < -100){
		percentage = -100;
	}else if (percentage > 100){
		percentage = 100;
	}else{
		percentage = percentage;
	}

	// fungsi untuk setting kecepatan setiap motor sesuai persentasi kecepatannya
	if (percentage<0){
		TM_GPIO_SetPinHigh(pin[1].GPIO, pin[1].Pin);
		TM_GPIO_SetPinLow(pin[0].GPIO, pin[0].Pin);
		percentage *= -1;
	}else {
		TM_GPIO_SetPinHigh(pin[0].GPIO, pin[0].Pin);
		TM_GPIO_SetPinLow(pin[1].GPIO, pin[1].Pin);
	}
	TM_PWM_SetChannelPercent(pwm.TIM_Data, pwm.Channel, percentage);
}
