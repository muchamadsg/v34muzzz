/*
 Example sketch for the PS3 Bluetooth library - developed by Kristian Lauszus
 For more information visit my blog: http://blog.tkjelectronics.dk/ or
 send me an e-mail:  kristianl@tkjelectronics.com
 */

#include <PS3BT.h>
#include <usbhub.h>
#include <Servo.h>


// Satisfy the IDE, which needs to see the include statment in the ino too.
#ifdef dobogusinclude
#include <spi4teensy3.h>
#endif
#include <SPI.h>

USB Usb;

BTD Btd(&Usb); // You have to create the Bluetooth Dongle instance like so
PS3BT PS3(&Btd, 0x03, 0xF4, 0xA4, 0x43, 0x5D, 0x2F); // This will also store the bluetooth address - this can be obtained from the dongle when running the sketch

#define DATALEN 8

// ------- BARU ------- //
/*Variabel Button Set*/
uint8_t buttonset1 = 0;
uint8_t buttonset2 = 0;
uint8_t data[8];
// ------- BARU ------- //

// ------- PIN/KONSTANTA ------- //
Servo servo1; // servo bawah
const int SERVO1 = 3;
Servo servo2; // servo atas
const int SERVO2 = 5;

int delayServo = 1000;
// ------- PIN/KONSTANTA ------- //



void setup() {
  Serial.begin(115200);
#if !defined(__MIPSEL__)
  while (!Serial); // Wait for serial port to connect - used on Leonardo, Teensy and other boards with built-in USB CDC serial connection
#endif
  if (Usb.Init() == -1) {
    Serial.print(F("\r\nOSC did not start"));
    while (1); //halt
  }
  Serial.print(F("\r\nPS3 Bluetooth Library Started"));

  //setup servo1, servo2, motor
  servo1.attach(SERVO1);
  servo2.attach(SERVO2);

  servo1.write(30);
  servo2.write(80);
}

void loop() {
  Usb.Task();
  
  if (PS3.PS3Connected){
    if (PS3.getButtonClick(PS)) {
      Serial.print(F("\r\nPS"));
      PS3.disconnect();
      Serial.flush();
    }
    
    data[0] = (PS3.getButtonPress(UP) << 5) | (PS3.getButtonPress(RIGHT) << 4) | (PS3.getButtonPress(DOWN) << 3) | (PS3.getButtonPress(LEFT) << 2) | (PS3.getButtonPress(START) << 1) | PS3.getButtonPress(SELECT);
    data[1] = (PS3.getButtonPress(TRIANGLE) << 5) | (PS3.getButtonPress(CIRCLE) << 4) | (PS3.getButtonPress(CROSS) << 3) | (PS3.getButtonPress(SQUARE) << 2) | (PS3.getButtonPress(L1) << 1) | PS3.getButtonPress(R1);
    data[2] = PS3.getAnalogHat(LeftHatX);
    data[3] = PS3.getAnalogHat(LeftHatY);
    data[4] = PS3.getAnalogHat(RightHatX);
    data[5] = PS3.getAnalogHat(RightHatY);
    data[6] = PS3.getAnalogButton(L2);
    data[7] = PS3.getAnalogButton(R2);

    for(int i=2; i<6; i++){
      if(data[i] > 117 && data[i] < 137){
        data[i] = 127;
      }
    }

    for(int i=6; i<8; i++){
      if(data[i] < 10){
        data[i] = 0;
      }
    }
    
    Serial.write(249);
    Serial.write(data, DATALEN);
//    Serial.println(data[1]);
    
    
    if(PS3.getButtonClick(SQUARE)){
      tembak();
//      Serial.println("XXXX");
    }
    else if(PS3.getButtonClick(CIRCLE)){
      tembak2();
    }
  }
}

void tembak()
{
   // buka-tutup servo bawah
  servo1.write(0);
  delay(delayServo);
  servo1.write(30);
  delay(delayServo);
  
  // buka-tutup servo atas
  servo2.write(50);
  delay(delayServo);
  servo2.write(80);
  delay(delayServo);
}
void tembak2()
{
   // buka-tutup servo atas bawah bareng
  servo1.write(0);
  servo2.write(50);
  delay(delayServo);
  servo1.write(30);
  servo2.write(80);
  delay(delayServo);
}
