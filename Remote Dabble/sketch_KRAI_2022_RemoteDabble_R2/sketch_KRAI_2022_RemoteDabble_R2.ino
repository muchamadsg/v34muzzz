#define CUSTOM_SETTINGS
#define INCLUDE_GAMEPAD_MODULE

#include "Dabble.h"
#include <Servo.h>

#define DATALEN 8

// ------- BARU ------- //
/*Variabel Button Set*/
uint8_t buttonset1 = 0;
uint8_t buttonset2 = 0;
uint8_t data[8];
// ------- BARU ------- //

// ------- PIN/KONSTANTA ------- //

// ------- PIN/KONSTANTA ------- //

// pin 9 to 3.3V(voltage divider) to RX bluetooth module
// pin 8 to TX bluetooth module

void setup() {
  Serial.begin(115200);
  Dabble.begin(9600, 8, 9); // Baud rate of  bluetooth module
}

void loop() {
  Dabble.processInput();
  
  data[0] = (GamePad.isUpPressed() << 5) | (GamePad.isRightPressed() << 4) | (GamePad.isDownPressed() << 3) | (GamePad.isLeftPressed() << 2) | (GamePad.isStartPressed() << 1) | GamePad.isSelectPressed();
  data[1] = (GamePad.isTrianglePressed() << 5) | (GamePad.isCirclePressed() << 4) | (GamePad.isCrossPressed() << 3) | (GamePad.isSquarePressed() << 2); // | (PS3.getButtonPress(L1) << 1) | PS3.getButtonPress(R1);
  data[2] = 0; // PS3.getAnalogHat(LeftHatX);
  data[3] = 0; // PS3.getAnalogHat(LeftHatY);ss
  data[4] = 0; // PS3.getAnalogHat(RightHatX);
  data[5] = 0; // PS3.getAnalogHat(RightHatY);
  data[6] = 0; // PS3.getAnalogButton(L2);
  data[7] = 0; // PS3.getAnalogButton(R2);

  
  Serial.write(249);
  Serial.write(data, DATALEN);
//  Serial.println(data[0]);
}
