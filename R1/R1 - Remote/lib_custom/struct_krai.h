#ifndef STRUCT_KRAI
#define STRUCT_KRAI

#include "tm_stm32f4_pwm.h"
#include "tm_stm32f4_gpio.h"

typedef struct {
	GPIO_TypeDef* GPIO;
	uint16_t Pin;
} pin_type;

typedef struct {
	TIM_TypeDef* TIM; // TIM2
	TM_PWM_TIM_t* TIM_Data; // &TIM2_Data
	TM_PWM_Channel_t Channel; //TM_PWM_Channel_2
	TM_PWM_PinsPack_t PinsPack; // TM_PWM_PinsPack_1
	double Timer;
} pwm_type;

#endif
