#include <stdio.h>
#include "stm32f4xx.h"
#include "attributes.h"
#include "defines.h"
#include "tm_stm32f4_gpio.h"
#include "tm_stm32f4_delay.h"
#include "tm_stm32f4_usart.h"

// Library sendiri
#include "struct_krai.h"
#include "gpio.h"
#include "motor.h"
//#include "motorbase.h"

int speed, pengangkat_speed;
float r1, r2, r3, r4;

// Variabel remote PS3
int datatest, dataarduino[8];
int datacounter, isdatareceived;

int segitiga, lingkaran, silang, kotak;
int up, right, down, left, start, selects;
int LX, LY, RX, RY;
int L1, R1, L2, R2;


TM_PWM_TIM_t TIM2_Data, TIM5_Data, TIM9_Data, TIM11_Data, TIM12_Data, TIM14_Data;

int main(void){

	SystemInit();
	TM_DELAY_Init();
	TM_USART_Init(USART1, TM_USART_PinsPack_1, 115200); // pin TX:A9 RX:A10

	// Motor 1
	pin_type motor1_pin[2] = {{GPIOB, GPIO_PIN_13}, {GPIOB, GPIO_PIN_11}};
	pwm_type motor1_pwm    = {TIM12, &TIM12_Data, TM_PWM_Channel_2, TM_PWM_PinsPack_1, 1000};
	motor_init(motor1_pwm, motor1_pin);

	// Motor 2
	pin_type motor2_pin[2] = {{GPIOA, GPIO_PIN_5}, {GPIOA, GPIO_PIN_3}};
	pwm_type motor2_pwm    = {TIM14, &TIM14_Data, TM_PWM_Channel_1, TM_PWM_PinsPack_1, 1000};
	motor_init(motor2_pwm, motor2_pin);

	// Motor 3
	pin_type motor3_pin[2] = {{GPIOE, GPIO_PIN_4}, {GPIOC, GPIO_PIN_14}};
	pwm_type motor3_pwm    = {TIM9, &TIM9_Data, TM_PWM_Channel_2, TM_PWM_PinsPack_2, 1000};
	motor_init(motor3_pwm, motor3_pin);

	// Motor 4
	pin_type motor4_pin[2] = {{GPIOB, GPIO_PIN_5}, {GPIOB, GPIO_PIN_7}};
	pwm_type motor4_pwm    = {TIM2, &TIM2_Data, TM_PWM_Channel_2, TM_PWM_PinsPack_2, 1000};
	motor_init(motor4_pwm, motor4_pin);

	// Pencapit A1 : (5,2,1) C3 C1
	pin_type pencapit_pin[2] = {{GPIOC, GPIO_PIN_3}, {GPIOC, GPIO_PIN_1}};
	pwm_type pencapit_pwm    = {TIM5, &TIM5_Data, TM_PWM_Channel_2, TM_PWM_PinsPack_1, 1000};
	motor_init(pencapit_pwm, pencapit_pin);

	// Pengangkat - B9 (11/1/1) E1 E3
	pin_type pengangkat_pin[2] = {{GPIOE, GPIO_PIN_1}, {GPIOE, GPIO_PIN_3}};
	pwm_type pengangkat_pwm    = {TIM11, &TIM11_Data, TM_PWM_Channel_1, TM_PWM_PinsPack_1, 1000};
	motor_init(pengangkat_pwm, pengangkat_pin);

	// Siap-siap sebelum jalan
//	motor_set_speed(motor1_pwm, motor1_pin, 0);
//	motor_set_speed(motor2_pwm, motor2_pin, 0);
//	motor_set_speed(motor3_pwm, motor3_pin, 0);
//	motor_set_speed(motor4_pwm, motor4_pin, 0);
//	motorbase_stop();
//	Delayms(2000);

    while(1)
    {
    	speed = 50;
    	pengangkat_speed = 50;

    	r1 = up - down + right - left + (0.5*lingkaran) - (0.5*kotak);
    	r2 = up - down - right + left - (0.5*lingkaran) + (0.5*kotak);
    	r3 = up - down + right - left - (0.5*lingkaran) + (0.5*kotak);
    	r4 = up - down - right + left + (0.5*lingkaran) - (0.5*kotak);

    	motor_set_speed(motor1_pwm, motor1_pin, r1*speed);
		motor_set_speed(motor2_pwm, motor2_pin, r2*(speed-5));
		motor_set_speed(motor3_pwm, motor3_pin, r3*speed);
		motor_set_speed(motor4_pwm, motor4_pin, r4*(speed-10));

//    	if(up){ // maju
//    		motor_set_speed(motor1_pwm, motor1_pin, up*speed);
//    		motor_set_speed(motor2_pwm, motor2_pin, up*speed);
//    		motor_set_speed(motor3_pwm, motor3_pin, up*speed);
//    		motor_set_speed(motor4_pwm, motor4_pin, up*speed);
//    	}else if(right){ // geser kanan
//    		motor_set_speed(motor1_pwm, motor1_pin, speed*right);
//        	motor_set_speed(motor2_pwm, motor2_pin, -speed*right);
//        	motor_set_speed(motor3_pwm, motor3_pin, speed*right);
//        	motor_set_speed(motor4_pwm, motor4_pin, -speed*right);
//		}else if(left){ // geser kiri
//			motor_set_speed(motor1_pwm, motor1_pin, -speed*left);
//	    	motor_set_speed(motor2_pwm, motor2_pin, speed*left);
//	    	motor_set_speed(motor3_pwm, motor3_pin, -speed*left);
//	    	motor_set_speed(motor4_pwm, motor4_pin, speed*left);
//		}else if(lingkaran){ // rotasi kanan, harusnya R1
//			motor_set_speed(motor1_pwm, motor1_pin, speed*lingkaran);
//	    	motor_set_speed(motor2_pwm, motor2_pin, -speed*lingkaran);
//	    	motor_set_speed(motor3_pwm, motor3_pin, -speed*lingkaran);
//	    	motor_set_speed(motor4_pwm, motor4_pin, speed*lingkaran);
//		}else if(kotak){ // rotasi kiri, harusnya L1
//			motor_set_speed(motor1_pwm, motor1_pin, -speed*kotak);
//	    	motor_set_speed(motor2_pwm, motor2_pin, speed*kotak);
//	    	motor_set_speed(motor3_pwm, motor3_pin, speed*kotak);
//	    	motor_set_speed(motor4_pwm, motor4_pin, -speed*kotak);
//		}else if(down){ // mundur
//			motor_set_speed(motor1_pwm, motor1_pin, -speed*down);
//	    	motor_set_speed(motor2_pwm, motor2_pin, -speed*down);
//	    	motor_set_speed(motor3_pwm, motor3_pin, -speed*down);
//	    	motor_set_speed(motor4_pwm, motor4_pin, -speed*down);
//		}else{
//			motor_set_speed(motor1_pwm, motor1_pin, 0);
//	    	motor_set_speed(motor2_pwm, motor2_pin, 0);
//	    	motor_set_speed(motor3_pwm, motor3_pin, 0);
//	    	motor_set_speed(motor4_pwm, motor4_pin, 0);
//		}

    	if(start){
    		motor_set_speed(pencapit_pwm, pencapit_pin, 100*start);
    	}else if(selects){
    		motor_set_speed(pencapit_pwm, pencapit_pin, -100*selects);
    	}else{
    		motor_set_speed(pencapit_pwm, pencapit_pin, 0);
    	}

    	if(segitiga){
    		motor_set_speed(pengangkat_pwm, pengangkat_pin, pengangkat_speed*segitiga);
    	}else if(silang){
    		motor_set_speed(pengangkat_pwm, pengangkat_pin, -pengangkat_speed*silang);
    	}else{
    		motor_set_speed(pengangkat_pwm, pengangkat_pin, 0);
    	}
    }
}

void TM_USART1_ReceiveHandler(uint8_t c){
	if(c == 249){
		isdatareceived = 1;
	}else if(isdatareceived){
		dataarduino[datacounter] = c;
		datacounter++;

		if(datacounter == 8){
			isdatareceived = 0;
			datacounter    = 0;
		}
	}

	up 	    = (dataarduino[0] >> 5) & 0x01;
	right   = (dataarduino[0] >> 4) & 0x01;
	down    = (dataarduino[0] >> 3) & 0x01;
	left    = (dataarduino[0] >> 2) & 0x01;
	start   = (dataarduino[0] >> 1) & 0x01;
	selects = (dataarduino[0] >> 0) & 0x01;

	segitiga  = (dataarduino[1] >> 5) & 0x01;
	lingkaran = (dataarduino[1] >> 4) & 0x01;
	silang    = (dataarduino[1] >> 3) & 0x01;
	kotak 	  = (dataarduino[1] >> 2) & 0x01;
	L1		  = (dataarduino[1] >> 1) & 0x01;
	R1		  = (dataarduino[1] >> 0) & 0x01;

	LX = dataarduino[2];
	LY = dataarduino[3];
	RX = dataarduino[4];
	RY = dataarduino[5];
	L2 = dataarduino[6];
	R2 = dataarduino[7];
}
