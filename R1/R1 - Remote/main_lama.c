#include <stdio.h>
#include "stm32f4xx.h"
#include "attributes.h"
#include "defines.h"
#include "tm_stm32f4_gpio.h"
#include "tm_stm32f4_delay.h"
#include "tm_stm32f4_usart.h"

// Library sendiri
#include "struct_krai.h"
#include "gpio.h"
#include "motor.h"
//#include "motorbase.h"

int speed;

// Variabel remote PS3
int datatest, dataarduino[8];
int datacounter, isdatareceived;

int segitiga, lingkaran, silang, kotak;
int up, right, down, left;
int LX, LY, RX, RY;
int L1, R1, L2, R2;


TM_PWM_TIM_t TIM2_Data, TIM4_Data, TIM9_Data, TIM12_Data;

int main(void){
	SystemInit();
	TM_DELAY_Init();
	TM_USART_Init(USART1, TM_USART_PinsPack_1, 115200); // pin TX:A9 RX:A10

	// Motor 1 - M5
	pin_type motor1_pin[2] = {{GPIOC, GPIO_PIN_1}, {GPIOC, GPIO_PIN_3}};
	pwm_type motor1_pwm    = {TIM2, &TIM2_Data, TM_PWM_Channel_2, TM_PWM_PinsPack_1, 1000};
	motor_init(motor1_pwm, motor1_pin);

	// Motor 2 - M3
	pin_type motor2_pin[2] = {{GPIOE, GPIO_PIN_4}, {GPIOC, GPIO_PIN_14}};
	pwm_type motor2_pwm    = {TIM9, &TIM9_Data, TM_PWM_Channel_2, TM_PWM_PinsPack_2, 1000};
	motor_init(motor2_pwm, motor2_pin);

	// Motor 3 - M1
	pin_type motor3_pin[2] = {{GPIOE, GPIO_PIN_3}, {GPIOE, GPIO_PIN_1}};
	pwm_type motor3_pwm    = {TIM4, &TIM4_Data, TM_PWM_Channel_4, TM_PWM_PinsPack_1, 1000};
	motor_init(motor3_pwm, motor3_pin);

	// Motor 4 - M7
	pin_type motor4_pin[2] = {{GPIOB, GPIO_PIN_13}, {GPIOB, GPIO_PIN_11}};
	pwm_type motor4_pwm    = {TIM12, &TIM12_Data, TM_PWM_Channel_2, TM_PWM_PinsPack_1, 1000};
	motor_init(motor4_pwm, motor4_pin);

	// Siap-siap sebelum jalan
//	motor_set_speed(motor1_pwm, motor1_pin, 0);
//	motor_set_speed(motor2_pwm, motor2_pin, 0);
//	motor_set_speed(motor3_pwm, motor3_pin, 0);
//	motor_set_speed(motor4_pwm, motor4_pin, 0);
//	motorbase_stop();
//	Delayms(2000);

    while(1)
    {
    	speed = 30;

    	if(up){ // maju
    		motor_set_speed(motor1_pwm, motor1_pin, up*speed);
    		motor_set_speed(motor2_pwm, motor2_pin, -up*speed);
    		motor_set_speed(motor3_pwm, motor3_pin, -up*speed);
    		motor_set_speed(motor4_pwm, motor4_pin, up*speed);
    	}else if(right){ // Geser kanan
    		motor_set_speed(motor1_pwm, motor1_pin, -speed*right);
        	motor_set_speed(motor2_pwm, motor2_pin, -speed*right);
        	motor_set_speed(motor3_pwm, motor3_pin, speed*right);
        	motor_set_speed(motor4_pwm, motor4_pin, speed*right);
		}else if(left){ // Geser kiri
			motor_set_speed(motor1_pwm, motor1_pin, speed*left);
	    	motor_set_speed(motor2_pwm, motor2_pin, speed*left);
	    	motor_set_speed(motor3_pwm, motor3_pin, -speed*left);
	    	motor_set_speed(motor4_pwm, motor4_pin, -speed*left);
		}else if(R1){ // Rotasi kanan
			motor_set_speed(motor1_pwm, motor1_pin, -speed*left);
	    	motor_set_speed(motor2_pwm, motor2_pin, -speed*left);
	    	motor_set_speed(motor3_pwm, motor3_pin, -speed*left);
	    	motor_set_speed(motor4_pwm, motor4_pin, -speed*left);
		}else if(L1){ // Rotasi kiri
			motor_set_speed(motor1_pwm, motor1_pin, speed*left);
	    	motor_set_speed(motor2_pwm, motor2_pin, speed*left);
	    	motor_set_speed(motor3_pwm, motor3_pin, speed*left);
	    	motor_set_speed(motor4_pwm, motor4_pin, speed*left);
		}else if(down){ // mundur
			motor_set_speed(motor1_pwm, motor1_pin, -speed*down);
	    	motor_set_speed(motor2_pwm, motor2_pin, speed*down);
	    	motor_set_speed(motor3_pwm, motor3_pin, speed*down);
	    	motor_set_speed(motor4_pwm, motor4_pin, -speed*down);
		}else{
			motor_set_speed(motor1_pwm, motor1_pin, 0);
	    	motor_set_speed(motor2_pwm, motor2_pin, 0);
	    	motor_set_speed(motor3_pwm, motor3_pin, 0);
	    	motor_set_speed(motor4_pwm, motor4_pin, 0);
		}

    }
}

void TM_USART1_ReceiveHandler(uint8_t c){
	if(c == 249){
		isdatareceived = 1;
	}else if(isdatareceived){
		dataarduino[datacounter] = c;
		datacounter++;

		if(datacounter == 8){
			isdatareceived = 0;
			datacounter    = 0;
		}
	}

	up 	  = (dataarduino[0] >> 5) & 0x01;
	right = (dataarduino[0] >> 4) & 0x01;
	down  = (dataarduino[0] >> 3) & 0x01;
	left  = (dataarduino[0] >> 2) & 0x01;

	segitiga  = (dataarduino[1] >> 5) & 0x01;
	lingkaran = (dataarduino[1] >> 4) & 0x01;
	silang    = (dataarduino[1] >> 3) & 0x01;
	kotak 	  = (dataarduino[1] >> 2) & 0x01;
	L1		  = (dataarduino[1] >> 1) & 0x01;
	R1		  = (dataarduino[1] >> 0) & 0x01;

	LX = dataarduino[2];
	LY = dataarduino[3];
	RX = dataarduino[4];
	RY = dataarduino[5];
	L2 = dataarduino[6];
	R2 = dataarduino[7];
}
