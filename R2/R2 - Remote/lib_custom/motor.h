#ifndef motor_h
#define motor_h

#include "struct_krai.h"

void motor_init(pwm_type pwm, pin_type pin[2]);
void motor_set_speed(pwm_type pwm, pin_type pin[2], float percentage);

#endif
