/*
 Example sketch for the PS3 Bluetooth library - developed by Kristian Lauszus
 For more information visit my blog: http://blog.tkjelectronics.dk/ or
 send me an e-mail:  kristianl@tkjelectronics.com
 */

#include <PS3BT.h>
#include <usbhub.h>

// Satisfy the IDE, which needs to see the include statment in the ino too.
#ifdef dobogusinclude
#include <spi4teensy3.h>
#endif
#include <SPI.h>

USB Usb;

BTD Btd(&Usb); // You have to create the Bluetooth Dongle instance like so
PS3BT PS3(&Btd);
//PS3BT PS3(&Btd, 0x03, 0xF4, 0xA4, 0x43, 0x5D, 0x2F); // dongle lama
//PS3BT PS3(&Btd, 0x00, 0x1A, 0x7D, 0xDA, 0x71, 0x10); // dongle baru

#define DATALEN 8

// ------- BARU ------- //
/*Variabel Button Set*/
uint8_t buttonset1 = 0;
uint8_t buttonset2 = 0;
uint8_t data[8];
// ------- BARU ------- //

void setup() {
  Serial.begin(115200);
#if !defined(__MIPSEL__)
  while (!Serial); // Wait for serial port to connect - used on Leonardo, Teensy and other boards with built-in USB CDC serial connection
#endif
  if (Usb.Init() == -1) {
    Serial.print(F("\r\nOSC did not start"));
    while (1); //halt
  }
  Serial.print(F("\r\nPS3 Bluetooth Library Started"));
}

void loop() {
  Usb.Task();
  
  if (PS3.PS3Connected){
    if (PS3.getButtonClick(PS)) {
      Serial.print(F("\r\nPS"));
      PS3.disconnect();
      Serial.flush();
    }
    
    data[0] = (PS3.getButtonPress(UP) << 5) | (PS3.getButtonPress(RIGHT) << 4) | (PS3.getButtonPress(DOWN) << 3) | (PS3.getButtonPress(LEFT) << 2) | (PS3.getButtonPress(START) << 1) | PS3.getButtonPress(SELECT);
    data[1] = (PS3.getButtonPress(TRIANGLE) << 5) | (PS3.getButtonPress(CIRCLE) << 4) | (PS3.getButtonPress(CROSS) << 3) | (PS3.getButtonPress(SQUARE) << 2) | (PS3.getButtonPress(L1) << 1) | PS3.getButtonPress(R1);
    data[2] = PS3.getAnalogHat(LeftHatX);
    data[3] = PS3.getAnalogHat(LeftHatY);
    data[4] = PS3.getAnalogHat(RightHatX);
    data[5] = PS3.getAnalogHat(RightHatY);
    data[6] = 0; //PS3.getAnalogButton(L2);
    data[7] = 0; //PS3.getAnalogButton(R2);

    for(int i=2; i<6; i++){
      if(data[i] > 117 && data[i] < 137){
        data[i] = 127;
      }
    }

    for(int i=6; i<8; i++){
      if(data[i] < 10){
        data[i] = 0;
      }
    }
    
//    Serial.write(249);
//    Serial.write(data, DATALEN);
    Serial.println(data[0]);
  }
}
