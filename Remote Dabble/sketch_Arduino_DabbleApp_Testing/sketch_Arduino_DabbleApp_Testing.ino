//for more u can click to bit.ly/gunberbagi
#define CUSTOM_SETTINGS
#define INCLUDE_GAMEPAD_MODULE

#include "Dabble.h"

//pin 9 to 3.3V(voltage divider) to RX bluetooth module
//pin 8 to TX bluetooth module

void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);
Dabble.begin(9600, 8, 9); // Baud rate of  bluetooth module

}

void loop() {
  // put your main code here, to run repeatedly:

  Dabble.processInput();
  if(Dabble.isAppConnected()){
    Serial.print("OK\t");
  }
  if(GamePad.isUpPressed()){
    Serial.println("UP");
  }
  if(GamePad.isDownPressed()){
    Serial.println("DOWN");
  }
  if(GamePad.isRightPressed()){
    Serial.println("RIGHT");
  }
  if(GamePad.isLeftPressed()){
    Serial.println("LEFT");
  }
  if(GamePad.isTrianglePressed()){
    Serial.println("SEGITIGA");
  }
  if(GamePad.isCirclePressed()){
    Serial.println("LINGKARAN");
  }
  if(GamePad.isCrossPressed()){
    Serial.println("SILANG");
  }
  if(GamePad.isSquarePressed()){
    Serial.println("KOTAK");
  }
  if(GamePad.isStartPressed()){
    Serial.println("START");
  }
  if(GamePad.isSelectPressed()){
    Serial.println("SELECT");
  }


}
