#include "tm_stm32f4_gpio.h"
#include "gpio.h"

// Kalau ModePin 1, berarti dia jadi output
// Kalau ModePin 0, berarti dia jadi input
gpio_init(GPIO_TypeDef* GPIO, uint16_t Pin, int ModePin){

	if(ModePin){
		TM_GPIO_Init(GPIO, Pin, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);
	}else{
		TM_GPIO_Init(GPIO, Pin, TM_GPIO_Mode_IN, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);
	}
}
