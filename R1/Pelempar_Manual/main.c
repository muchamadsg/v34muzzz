#include "stm32f4xx.h"
#include "tm_stm32f4_gpio.h"
#include "tm_stm32f4_pwm.h"
#include "tm_stm32f4_timer_properties.h"
#include "tm_stm32f4_delay.h"
#include "stm32f4xx_tim.h"

TM_PWM_TIM_t TIM2_Data;
//TM_PWM_TIM_t TIM9_Data;

int cross;
int square;

int main(void) {
    SystemInit();
    TM_DELAY_Init();
    TM_GPIO_Init(GPIOB, GPIO_Pin_5, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL,
    		TM_GPIO_Speed_High);

    TM_GPIO_Init(GPIOB, GPIO_Pin_7,TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL,
        	TM_GPIO_Speed_Low);

    TM_PWM_InitTimer(TIM2, &TIM2_Data, 1000);
    TM_PWM_InitChannel(&TIM2_Data, TM_PWM_Channel_2, TM_PWM_PinsPack_2);

    TM_PWM_SetChannelPercent(&TIM2_Data, TM_PWM_Channel_2, 10);
    Delayms(500);
    TM_PWM_SetChannelPercent(&TIM2_Data, TM_PWM_Channel_2, 20);
    Delayms(500);
    TM_PWM_SetChannelPercent(&TIM2_Data, TM_PWM_Channel_2, 30);
    Delayms(500);
	TM_PWM_SetChannelPercent(&TIM2_Data, TM_PWM_Channel_2, 40);
	Delayms(500);
    TM_PWM_SetChannelPercent(&TIM2_Data, TM_PWM_Channel_2, 50);
    Delayms(500);
    TM_PWM_SetChannelPercent(&TIM2_Data, TM_PWM_Channel_2, 60);
    Delayms(500);
	TM_PWM_SetChannelPercent(&TIM2_Data, TM_PWM_Channel_2, 70);
	Delayms(500);
    while(1){
    	TM_GPIO_SetPinValue(GPIOB,GPIO_Pin_5,1);
  TM_GPIO_SetPinValue(GPIOB,GPIO_Pin_7,0);

  TM_PWM_SetChannelPercent(&TIM2_Data, TM_PWM_Channel_2, 80);
}
}
