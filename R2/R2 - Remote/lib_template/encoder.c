#include "tm_stm32f4_exti.h"
#include "tm_stm32f4_gpio.h"
#include "tm_stm32f4_delay.h"
#include <math.h>
#include "defines.h"

int count = 0;

void encoder_Init(){
	TM_EXTI_Attach(GPIOD, GPIO_Pin_0, TM_EXTI_Trigger_Rising);
	TM_GPIO_Init(GPIOD, GPIO_Pin_2, TM_GPIO_Mode_IN, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);
	count = 0;
}

void encoder_read(){
	if(TM_GPIO_GetInputPinValue(GPIOD, GPIO_Pin_2) == 0){
		count++;
	}else{
		count--;
	}
}

int encoder_display(){
	return count;
}

void TM_EXTI_Handler(int GPIO_Pin){
	if(GPIO_Pin == GPIO_Pin_0){
		encoder_read();
	}
}
