#include "stm32f4xx.h"
#include "tm_stm32f4_gpio.h"
#include "tm_stm32f4_delay.h"

#include "struct_krai.h"
#include "gpio.h"
#include "motor.h"

int posisi_pneumatic; // maksimal 4 sekon, pada posisi panjang max

void ubah_pneumatic(int pos);

// Pin pneumatic masih asal
pin_type pne_pin[3] = {{GPIOA, GPIO_Pin_0}, {GPIOA, GPIO_Pin_1}, {GPIOA, GPIO_Pin_2}};

int main(void)
{
	SystemInit();
	TM_DELAY_Init();

	gpio_init(pne_pin[0].GPIO, pne_pin[0].Pin, 1);
	gpio_init(pne_pin[1].GPIO, pne_pin[0].Pin, 1);
	gpio_init(pne_pin[2].GPIO, pne_pin[0].Pin, 1); // selalu HIGH

	// Panjangin full dahulu, set posisi = 0
	posisi_pneumatic = 4000;
	TM_GPIO_SetPinValue(pne_pin[0].GPIO, pne_pin[1].Pin, 1);
	TM_GPIO_SetPinValue(pne_pin[1].GPIO, pne_pin[1].Pin, 0);
	TM_GPIO_SetPinValue(pne_pin[2].GPIO, pne_pin[1].Pin, 1); // selalu HIGH
	Delayms(4000);

	// Berhenti
	TM_GPIO_SetPinValue(pne_pin[0].GPIO, pne_pin[1].Pin, 0);
	TM_GPIO_SetPinValue(pne_pin[1].GPIO, pne_pin[1].Pin, 0);
	TM_GPIO_SetPinValue(pne_pin[2].GPIO, pne_pin[1].Pin, 0); // selalu HIGH

    while(1)
    {
        ubah_pneumatic(2000);
        Delayms(1000);
        ubah_pneumatic(0000);
        Delayms(1000);
        ubah_pneumatic(2000);
        Delayms(1000);
        ubah_pneumatic(4000);
        Delayms(1000);
    }
}

void ubah_pneumatic(int pos){
	int target = pos - posisi_pneumatic;
	posisi_pneumatic += target;

	if(target > 0){ // Pneumatic memanjang selama target
		TM_GPIO_SetPinValue(pne_pin[0].GPIO, pne_pin[1].Pin, 1);
		TM_GPIO_SetPinValue(pne_pin[1].GPIO, pne_pin[1].Pin, 0);
		TM_GPIO_SetPinValue(pne_pin[2].GPIO, pne_pin[1].Pin, 1);
		Delayms(target);

		TM_GPIO_SetPinValue(pne_pin[0].GPIO, pne_pin[1].Pin, 0);
		TM_GPIO_SetPinValue(pne_pin[1].GPIO, pne_pin[1].Pin, 0);
		TM_GPIO_SetPinValue(pne_pin[2].GPIO, pne_pin[1].Pin, 0);
	}else if(target < 0){ // Pneumatic memendek selama target
		TM_GPIO_SetPinValue(pne_pin[0].GPIO, pne_pin[1].Pin, 0);
		TM_GPIO_SetPinValue(pne_pin[1].GPIO, pne_pin[1].Pin, 1);
		TM_GPIO_SetPinValue(pne_pin[2].GPIO, pne_pin[1].Pin, 1);
		Delayms(target);

		TM_GPIO_SetPinValue(pne_pin[0].GPIO, pne_pin[1].Pin, 0);
		TM_GPIO_SetPinValue(pne_pin[1].GPIO, pne_pin[1].Pin, 0);
		TM_GPIO_SetPinValue(pne_pin[2].GPIO, pne_pin[1].Pin, 0);
	}
}
