// Include header
#include "motorbase.h"

// Include library template
#include "tm_stm32f4_gpio.h"
#include "tm_stm32f4_delay.h"

// Include library sendiri
#include "struct_krai.h"
#include "gpio.h"
#include "motor.h"

TM_PWM_TIM_t TIM9_Data, TIM12_Data, TIM2_Data, TIM14_Data;

pin_type motor1_pin[2], motor2_pin[2], motor3_pin[2], motor4_pin[2];
pwm_type motor1_pwm, motor2_pwm, motor3_pwm, motor4_pwm;

void motorbase_init(){

	// Motor 1
//	pin_type motor1_pin[2] = {{GPIOE, GPIO_PIN_4}, {GPIOC, GPIO_PIN_14}};
//	pwm_type motor1_pwm    = {TIM9, &TIM9_Data, TM_PWM_Channel_2, TM_PWM_PinsPack_2, 1000};
//	motor_init(motor1_pwm, motor1_pin);

	motor1_pin[0].GPIO = GPIOE;
	motor1_pin[0].Pin  = GPIO_PIN_4;
	motor1_pin[1].GPIO = GPIOC;
	motor1_pin[1].Pin  = GPIO_PIN_14;

	motor1_pwm.TIM 		= TIM9;
	motor1_pwm.TIM_Data = &TIM9_Data;
	motor1_pwm.Channel  = TM_PWM_Channel_2;
	motor1_pwm.PinsPack = TM_PWM_PinsPack_2;
	motor1_pwm.Timer    = 1000;

	motor_init(motor1_pwm, motor1_pin);

	// Motor 2
//	pin_type motor2_pin[2] = {{GPIOB, GPIO_PIN_13}, {GPIOB, GPIO_PIN_11}};
//	pwm_type motor2_pwm    = {TIM12, &TIM12_Data, TM_PWM_Channel_2, TM_PWM_PinsPack_1, 1000};
//	motor_init(motor2_pwm, motor2_pin);

	motor1_pin[0].GPIO = GPIOB;
	motor1_pin[0].Pin  = GPIO_PIN_13;
	motor1_pin[1].GPIO = GPIOB;
	motor1_pin[1].Pin  = GPIO_PIN_11;

	motor1_pwm.TIM 		= TIM12;
	motor1_pwm.TIM_Data = &TIM12_Data;
	motor1_pwm.Channel  = TM_PWM_Channel_2;
	motor1_pwm.PinsPack = TM_PWM_PinsPack_1;
	motor1_pwm.Timer    = 1000;

	motor_init(motor2_pwm, motor2_pin);

	// Motor 3
//	pin_type motor3_pin[2] = {{GPIOC, GPIO_PIN_3}, {GPIOC, GPIO_PIN_1}};
//	pwm_type motor3_pwm    = {TIM2, &TIM2_Data, TM_PWM_Channel_2, TM_PWM_PinsPack_1, 1000};
//	motor_init(motor3_pwm, motor3_pin);

	motor1_pin[0].GPIO = GPIOC;
	motor1_pin[0].Pin  = GPIO_PIN_3;
	motor1_pin[1].GPIO = GPIOC;
	motor1_pin[1].Pin  = GPIO_PIN_1;

	motor1_pwm.TIM 		= TIM2;
	motor1_pwm.TIM_Data = &TIM2_Data;
	motor1_pwm.Channel  = TM_PWM_Channel_2;
	motor1_pwm.PinsPack = TM_PWM_PinsPack_1;
	motor1_pwm.Timer    = 1000;

	motor_init(motor3_pwm, motor3_pin);

	// Motor 4
//	pin_type motor4_pin[2] = {{GPIOA, GPIO_PIN_5}, {GPIOA, GPIO_PIN_3}};
//	pwm_type motor4_pwm    = {TIM14, &TIM14_Data, TM_PWM_Channel_1, TM_PWM_PinsPack_1, 1000};
//	motor_init(motor4_pwm, motor4_pin);

	motor1_pin[0].GPIO = GPIOA;
	motor1_pin[0].Pin  = GPIO_PIN_5;
	motor1_pin[1].GPIO = GPIOA;
	motor1_pin[1].Pin  = GPIO_PIN_3;

	motor1_pwm.TIM 		= TIM14;
	motor1_pwm.TIM_Data = &TIM14_Data;
	motor1_pwm.Channel  = TM_PWM_Channel_1;
	motor1_pwm.PinsPack = TM_PWM_PinsPack_1;
	motor1_pwm.Timer    = 1000;

	motor_init(motor4_pwm, motor4_pin);
}

void motorbase_maju(int speed){
	motor_set_speed(motor1_pwm, motor1_pin, speed);
	motor_set_speed(motor2_pwm, motor2_pin, speed);
	motor_set_speed(motor3_pwm, motor3_pin, speed);
	motor_set_speed(motor4_pwm, motor4_pin, speed);
}

void motorbase_mundur(int speed){

}

void motorbase_kanan(int speed){

}

void motorbase_kiri(int speed){

}

void motorbase_putarkanan(int speed){

}

void motorbase_putarkiri(int speed){

}

void motorbase_stop(){
	motor_set_speed(motor1_pwm, motor1_pin, 0);
	motor_set_speed(motor2_pwm, motor2_pin, 0);
	motor_set_speed(motor3_pwm, motor3_pin, 0);
	motor_set_speed(motor4_pwm, motor4_pin, 0);
}
